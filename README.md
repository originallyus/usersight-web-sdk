# UserSight SDK for Web

UserSight SDK is a javascript library to capture users' feedbacks via web browser.

## Installation

- basic jQuery library is required (compatible with all jQuery verions)
- import Javascript (you may also manually download this file from our backend)

```html
<script src="https://web.usersight.io/usersight.min.js"></script>
```

## Usage

This code will show the feedback/rating form depends on configuration from admin portal.

Please contact us for the App Secret and PackageID specific to your website.

```javascript
//Optional: obtain this from somewhere else in your project
var userId = 'Member987654';

//Optional: tag this to a certain event
var eventTag = 'transaction_completed';

//Optional: choose a language (en, th, ms, ta)
var lang = 'en';

//Optional: use an alternative form configured in CMS
var formSlug = "nps-1";

//Optional: provide your own unique Device UUID
var deviceUuid = 'e86f8918-e18c-4b45-a9d6-2290a9643322';

//Optional: obtain this from somewhere else in your project
var metadata = 'transaction_id_987654321';

UserSightSDK.Initialize({
    appSecret: "xxxxxxxxxxxxxxxx",
    packageId: "xx.xxxxxxxx.xxxxx",
    debug: false,
    language: lang,					// optional
    userId: userId,					// optional
    eventTag: eventTag,				// optional
    formSlug: formSlug,				// optional
    deviceUuid: deviceUuid,			// optional
    metadata: metadata,             // optional
}, function(formDidShow) {
    //Optional callback function
    console.log(formDidShow ? "Form was shown" : "No form was shown");
});
```

## Debug vs Production mode

In Debug mode **(debug = true)**, the rating form is always shown, for debugging purpose.

In Production mode **(debug = false)**, the rating form may not be shown all the time, ie. nothing happens when the code is executed. This is not a bug.

The form might only be shown only once per hour, per day, per week, etc.

## Delayed triggered

A simple Javascript `setTimeout()` function can be used to delay the invocation of the SDK and hence delay the showing of the form.

```javascript
//Initialize script
var initUserSightSDK = function (debug) {
    UserSightSDK.Initialize({
        appSecret: "xxxxxxxxxxxxxxxx",
        packageId: "xx.xxxxxxxx.xxxxx",
        debug: debug,
        eventTag: eventTag,				// optional
        userId: userId, 				// optional
    }, function(formDidShow){
        //Optional callback function
        console.log(formDidShow ? "UserSight was presented to user" : "UserSight was not presented to user");
    });
};

//elsewhere in your website's code
setTimeout(function() {initUserSightSDK(true);}, 5000)
```


## Callback Function (optional)

An optional callback function can be provided to capture the outcome of the SDK. See code sample above.

  - **formDidShow = false** is triggered when there is no form to be shown, this callback function will be triggered almost immediately.
  - **formDidShow = true** is triggered when has been shown and user has finished interacting with it or dismiss/cancel it



## Other Functions

Retrieve the version number of the SDK

```javascript
const version = UserSightSDK.GetVersion();
console.log("UserSightSDK Version:", UserSightSDKVersion);
```

Retrieve the internal status whether a Submit action has been done.

```javascript
const didSubmit = UserSightSDK.GetDidSubmit();
console.log("UserSightSDK Did Submit:", didSubmit);
```

Retrieve the user-selected rating number. This will returns 0 as default when user has not selected any numeric rating, or forms that does not have any numeric selection such as Comment, Poll, External Survey. The valid value is 1-5 for Satisfaction & CES form, 1-10 for NPS form. Do note that this number is updated in real-time as user clicks on the rating numbers.

```javascript
const ratingNumber = UserSightSDK.GetRatingNumber();
console.log("UserSightSDK Rating Number:", ratingNumber);
```